import * as React from "react";
import styled, { css } from "styled-components";
import "../layout.css";
import SpinningLogo from "../components/SpinningLogo";
import FerrisWheel from "../components/FerrisWheel";

const layout = css`
  display: flex;
  flex-direction: column;
  align-items: center;

  ${FerrisWheel} {
    margin: 4rem 0;
  }

  h1 {
    margin-bottom: 4rem;
    color: #222;
  }

  .logo {
    width: 100%;
    max-width: 300px;

    animation: spin 3s linear infinite;
  }

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }

    to {
      transform: rotate(-360deg);
    }
  }
`;

const IndexPage = ({ className }) => {
  return (
    <div className={className}>
      <h1>The People's Parrot Party</h1>
      <SpinningLogo />
      <FerrisWheel />
    </div>
  );
};

export default styled(IndexPage)`
  ${layout}
`;
