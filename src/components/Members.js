import * as React from "react";
import styled, { css } from "styled-components";

const MemberLink = styled.a`
  text-decoration: none;
  color: inherit;
`;

const StyledMember = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;

  img {
    border-radius: 50%;
    height: 81px;
    width: 81px;
  }

  :hover {
    :after {
      content: "${({ name }) => name}";
      position: absolute;
      bottom: -24px;
      background: white;
      padding: 2px 8px;
      border-radius: 2px;
      box-shadow: 0 2px 5px rgba(0, 0, 0, 0.2);
    }
  }
`;

export const Member = styled(({ name, picture, className, link }) => {
  if (link) {
    return (
      <div className={className}>
        <MemberLink className="member-link" href={link}>
          <StyledMember name={name}>
            <img src={picture} alt={name} />
          </StyledMember>
        </MemberLink>
      </div>
    );
  }

  return (
    <div className={className}>
      <StyledMember name={name}>
        <img src={picture} alt={name} />
      </StyledMember>
    </div>
  );
})``;
