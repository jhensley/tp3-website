import React from "react";
import styled, { css } from "styled-components";
import alec from "../images/critical-thinking-alec.png";
import elijah from "../images/sun-elijah.png";
import jordan from "../images/pontificating-jordan.png";
import stuart from "../images/stuart.png";
import { Member } from "./Members";

const layout = css`
  display: flex;
  align-items: center;

  position: relative;
`;

const FerrisWheelBottom = styled.div`
  height: 40px;
  width: 81px;
  background: #b03041;

  position: absolute;
  bottom: -60px;
  left: calc(50% - 40px);
  border-radius: 20px 20px 4px 4px;
`;

const FerrisWheelStand = styled.div`
  height: 220px;
  width: 41px;
  background: #cb4154;

  position: absolute;
  top: calc(50% - 20px);
  left: calc(50% - 20px);

  border-top-left-radius: 100px;
  border-top-right-radius: 100px;
`;

const FerrisWheelFrame = styled.div`
  height: 300px;
  width: 300px;
  background: transparent;
  border: 3px solid #333;
  border-radius: 50%;

  position: relative;

  ${Member} {
    position: absolute;
    animation: spin 9s linear infinite reverse;
    transform-origin: center;
  }

  .jordan {
    top: calc(50% - 40px);
    left: -40px;
  }

  .stuart {
    top: calc(50% - 40px);
    right: -40px;
  }

  .alec {
    top: -40px;
    right: calc(50% - 40px);
  }

  .elijah {
    bottom: -40px;
    right: calc(50% - 40px);
  }

  animation: spin 9s linear infinite;
`;

const Spoke = styled.span`
  width: 300px;
  height: 3px;
  background: #333;

  position: absolute;
  top: calc(50% + 1px);

  transform: rotate(${({ angle }) => angle}deg);
`;

const FerrisWheel = ({ className }) => {
  return (
    <div className={className}>
      <FerrisWheelFrame>
        <Spoke angle={0} />
        <Spoke angle={45} />
        <Spoke angle={90} />
        <Spoke angle={135} />
        <Member className="jordan" name="Jordan" picture={jordan} />
        <Member className="elijah" name="Elijah" picture={elijah} />
        <Member
          className="alec"
          name="Alec"
          picture={alec}
          link="https://github.com/alecgirman"
        />
        <Member className="stuart" name="Stuart" picture={stuart} />
      </FerrisWheelFrame>
      <FerrisWheelStand />
      <FerrisWheelBottom />
    </div>
  );
};

export default styled(FerrisWheel)`
  ${layout}
`;
