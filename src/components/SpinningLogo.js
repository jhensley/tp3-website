import * as React from "react";
import styled, { css } from "styled-components";
import p3Logo from "../images/P3_Logo_4_color.svg";
import { OutlineButton } from "./ui/Button";
import { useState } from "react";

const layout = css`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Logo = styled.img`
  width: 100%;
  max-width: 300px;
  margin-bottom: 32px;
  animation: spin ${({ duration }) => duration}s linear infinite;
`;

const SpinningLogo = ({ className }) => {
  const [isTerminalVelocity, setTerminalVelocity] = useState(false);
  const [rotationDuration, setRotationDuration] = useState(6);

  const speedUp = () => {
    if (rotationDuration <= 0.005) {
      setTerminalVelocity(true);
      return;
    }

    setRotationDuration(rotationDuration * 0.8);
  };

  return (
    <div className={className}>
      <Logo duration={rotationDuration} src={p3Logo} alt="p3 Logo" />

      {isTerminalVelocity ? (
        <b>OH GOD PLEASE STOP! I'M GONNA PUKE!!</b>
      ) : (
        <OutlineButton onClick={speedUp}>Faster...</OutlineButton>
      )}
    </div>
  );
};

export default styled(SpinningLogo)`
  ${layout}
`;
