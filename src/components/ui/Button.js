import styled from "styled-components";

export const Button = styled.button`
  border-radius: 5px;
  height: 40px;
  padding: 8px 16px;
  cursor: pointer;
`;

export const OutlineButton = styled(Button)`
  background: white;
  color: #333;
  border: 2px solid #333;

  :active {
    background: #eee;
  }
`;
